**Readme**
----------
#.	**Autor:** C.A. Francisco Carrasco Flores
#.	**Tema :** Tarea 3 LDM. Presentaci�n tema libre. (Tema escogido: MI ciudad: Ronda)
#.      **Instrucciones** para generar la presentaci�n:

* Abra el documento index.html en su navegador para iniciar la presentaci�n web. No precisa de conexi�n a internet
* La realizaci�n de trabajo se ha realizado en un equipo con las siguientes caracter�sticas que podr�an afectarle a la correcta reproducci�n si esta var�a en su equipo:</p>
* Resoluci�n de pantalla 1440x900-60Hz.
* Navegador Internet Explorer 8.0
* (s�lo precisa Internet para la visualizaci�n del v�deo de la p�gina principal- se ha creado un enlace externo-.)
* Las *im�genes* se encuentran ubicadas en la carpeta (d/i) por lo que se cargar�n automaticamente.
* El *audio* se encuentra ubicado en la carpeta (d/audio) por lo que se cargar� automaticamente.
* Se ha utilizado para la creaci�n de la tarea lenguaje de marcas *Html* y algo de *CCS*.